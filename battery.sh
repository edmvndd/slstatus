#!/bin/sh

battery=$(acpi -b | awk -F ',' '{print $2}' | sed 's/ //;s/%//')

if [ $battery -gt 75 ]; then
	echo "[   $battery% "
elif [ $battery -gt 50 ]; then
	echo "[   $battery% "
elif [ $battery -gt 25 ]; then
	echo "[   $battery% "
else
	echo "[  Low battery  : $battery% "
	dunstify -t 3000 "Low battery!"
fi
